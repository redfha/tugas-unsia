#include <iostream>
#include <string>
using namespace std;

int main(){
  string kota[7] = {"Jakarta", "Malang", "Surabaya", "Bandung", "Garut", "Depok", "Tanggerang"};

  for (int i = 1; i < 7; i++) {
        char key = kota[i].at(0);
        string nama_kota = kota[i];
        int j = i - 1;
        while (j >= 0 && kota[j].at(0) > key) {
            kota[j + 1] = kota[j];
            j = j - 1;
        }
        kota[j + 1] = nama_kota;
    }
  
  cout << "Sorted array: \n";
  for(int i = 0; i < 7; i++){
    cout << i+1 << ". " << kota[i] << endl;
  }

  return 0;
}