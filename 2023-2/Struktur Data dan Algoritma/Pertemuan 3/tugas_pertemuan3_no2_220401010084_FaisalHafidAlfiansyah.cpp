#include <iostream>
using namespace std;
int main(){
    int nilai_tes_coding;
    char nilai_interview;
    string status_penilaian_tes_coding, status_penilaian_interview;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "|                 Sistem Penilaian Penerimaan Calon Programmer                 |\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << " Masukkan nilai tes coding\t: ";
    cin >> nilai_tes_coding;
    cout << " Masukkan nilai interview\t: ";
    cin >> nilai_interview;
    if(nilai_tes_coding > 80){
        status_penilaian_tes_coding = "LOLOS";
    }else if (nilai_tes_coding >= 60){
        status_penilaian_tes_coding = "DIPERTIMBANGKAN";
    }else{
        status_penilaian_tes_coding = "GAGAL";
    }
    if(nilai_interview == 'A' || nilai_interview == 'B'){
        status_penilaian_interview = "LOLOS";
    }else{
        status_penilaian_interview = "GAGAL";
    }
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    if((status_penilaian_tes_coding == "LOLOS" || status_penilaian_tes_coding == "DIPERTIMBANGKAN") && status_penilaian_interview == "LOLOS"){
        cout << " Selamat Kamu Berhasil Menjadi Calon Programmer\n";
    }else{
        cout << " Maaf Kamu Belum Berhasil Menjadi Calon Programmer\n";
    }
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    return 0;
}