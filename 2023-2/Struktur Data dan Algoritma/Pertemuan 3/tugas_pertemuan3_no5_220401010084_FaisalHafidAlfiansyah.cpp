#include <iostream>
using namespace std;
int main(){
    int umur, tinggi_badan;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "|                               Tarif Disney Island                            |\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << " Masukkan umur (tahun)\t\t: ";
    cin >> umur;
    cout << " Masukkan tinggi badan (cm)\t: ";
    cin >> tinggi_badan;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    if (umur < 1){
        cout << " Dilarang masuk!";
    }else if(umur < 3){
        if(umur >= 2 && tinggi_badan > 70){
            cout << " Tarif: Rp40.000";
        }else{
            cout << " Tarif: Rp30.000";
        }
    }else if(umur < 7){
        if (umur >= 4 && tinggi_badan > 120){
            cout << " Tarif: Rp55.000";
        }else{
            cout << " Tarif: Rp40.000";
        }
    }else if(umur < 10){
        if(umur >= 8 && tinggi_badan > 150){
            cout << " Tarif: Rp70.000";
        }else{
            cout << " Tarif: Rp50.000";
        }
    }else{
        cout << " Tarif: Rp80.000";
    }
    cout << "\n-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    return 0;
}