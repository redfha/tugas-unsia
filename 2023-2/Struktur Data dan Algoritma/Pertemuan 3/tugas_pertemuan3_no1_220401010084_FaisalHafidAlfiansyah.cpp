#include <iostream>
using namespace std;
int main(){
    string pangkat, nama, tempat_tinggal;
    int umur, jumlah_tabungan;

    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "|                     Program Klasifikasi Keanggotaan Mafia                    |\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "Nama \t\t\t\t: ";
    getline(cin >> ws, nama);
    cout << "Umur (tahun)\t\t\t: ";
    cin >> umur;
    cout << "Tempat Tinggal \t\t\t: ";
    getline(cin >> ws, tempat_tinggal);
    cout << "Uang Tabungan dalam dollar \t: ";
    cin >> jumlah_tabungan;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    if (umur > 40 && (tempat_tinggal == "Nevada" || tempat_tinggal == "New York" || tempat_tinggal == "Havana" ) && jumlah_tabungan >= 10000000){
        pangkat = "Don";
        cout << nama << " kemungkinan adalah seorang anggota mafia dengan " << pangkat << "." << endl;
    }else if (umur >= 25 && (tempat_tinggal == "New Jersey" || tempat_tinggal == "Manhattan" || tempat_tinggal == "Nevada" ) && (jumlah_tabungan >= 1000000 && jumlah_tabungan <= 2000000)){
        pangkat = "Underboss";
        cout << nama << " kemungkinan adalah seorang anggota mafia dengan " << pangkat << "." << endl;
    }else if (umur >= 18 && (tempat_tinggal == "California" || tempat_tinggal == "Detroit" || tempat_tinggal == "Boston" ) && jumlah_tabungan < 1000000){
        pangkat = "Capo";
        cout << nama << " kemungkinan adalah seorang anggota mafia dengan " << pangkat << "." << endl;
    }else{
        cout <<  nama << " tidak mencurigakan.\n";
    }
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    return 0;
}