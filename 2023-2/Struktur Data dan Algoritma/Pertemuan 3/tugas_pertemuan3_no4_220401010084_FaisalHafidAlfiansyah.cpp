#include <iostream>
using namespace std;
int main(){
    int waktu_lari, waktu_pushup, waktu_plank, total_kalori_terbakar;
    total_kalori_terbakar = 0;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "|                                 Program Penghitung Jumlah Kalori Terbakar Setelah Olah Raga                          |\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << " Masukkan berapa lama Anda lari. Masukkan \"0\" jika tidak melakukannya (menit) : ";
    cin >> waktu_lari;
    cout << " Masukkan berapa lama Anda push-up. Masukkan \"0\" jika tidak melakukannya (menit) : ";
    cin >> waktu_pushup;
    cout << " Masukkan berapa lama Anda plank. Masukkan \"0\" jika tidak melakukannya (menit) : ";
    cin >> waktu_plank;
    if(waktu_lari >= 5){
        total_kalori_terbakar += (waktu_lari / 5) * 60;
    }
    if(waktu_pushup >= 30){
        total_kalori_terbakar += (waktu_pushup / 30) * 200;
    }
    if(waktu_plank >= 1){
        total_kalori_terbakar += waktu_plank * 5;
    }
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << " Total Kalori terbakar sebanyak " << total_kalori_terbakar << " Kalori.\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    return 0;
}