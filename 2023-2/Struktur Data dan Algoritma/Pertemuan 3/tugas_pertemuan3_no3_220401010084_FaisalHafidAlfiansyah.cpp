#include <iostream>
using namespace std;
int main(){
    int nomor;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << "|                 Sistem Pengaturan Nomor Punggung PERSEGI FC                  |\n";
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    cout << " Masukkan Angka\t: ";
    cin >> nomor;
    cout << "-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    if(nomor % 2 == 0){
        cout << " Positon: Target Attacker.";
        if(nomor >= 50 || nomor <= 100){
            cout << " Berhak dipilih menjadi capten team.\n";
        }else{
            cout << "\n";
        }
    }else{
        if(nomor > 90){
            if(nomor % 3 == 0 || nomor % 5 == 0){
                cout << " Positon: Defender, Playmaker, Keeper.";
            }else{
                cout << " Positon: Defender, Playmaker.";
            }
        }else if(nomor % 3 == 0 || nomor % 5 == 0){
            cout << " Positon: Defender, Keeper.";
        }else{
            cout << " Positon: Defender.";
        }
    }    
    cout << "\n-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-\n";
    return 0;
}