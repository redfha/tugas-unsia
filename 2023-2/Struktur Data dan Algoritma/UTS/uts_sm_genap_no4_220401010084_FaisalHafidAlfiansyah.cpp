#include <iostream>
using namespace std;

#define QUEUE_SIZE 5                // ukuran antrian

class Queue{
    private:
        int ifront;                   // var untuk index nilai paling depan
        int iback;                    // var untuk index nilai yang terakhir di push
        int queue[QUEUE_SIZE];        // var untuk mendefinisikan ukuran queue
    
    public:
        Queue(){
            ifront = -1;
            iback = -1;
        };

    // method
    void enqueue(int element){
        if(isFull()){
            cout << "Antrian penuh. Tidak dapat menahkan antrian lagi." << endl;
        }else {
            if(ifront == -1){
                ifront = 0;
            }
            iback = (iback + 1) % QUEUE_SIZE;
            queue[iback] = element;
            cout << "Elemen " << element << " berhasil ditambah ke antrian." << endl;
        }
    }
    void dequeue(){
        if(isEmpty()){
            cout << "Antrian kosong. Tidak dapat menghapus antrian" << endl;
        }else {
            int element;
            element = queue[ifront];
            ifront = (ifront + 1) % QUEUE_SIZE;
            cout << "Elemen " << element << " berhasil dihapus dari antrian." << endl;
        }
    }
    bool isEmpty(){
        if(ifront == -1){
            return true;
        }else {
            return false;
        }
    }
    
    bool isFull(){
        if((ifront == 0 && iback == QUEUE_SIZE - 1) || (ifront == iback + 1)){
            return true;
        }else {
            return false;
        }
    }
    void display(){
        if (isEmpty()){
            cout << "Antrian Kosong." << endl;
        } else {
            cout << "Isi antrian : ";
            int i;
            for (i = ifront; i != iback; i = (i + 1) % QUEUE_SIZE){
                cout << queue[i] << " ";
            }
            cout << queue[i] << endl;
        }
    }  
};

int main() {
    Queue q;
    q.enqueue(3);
    q.enqueue(7);
    q.dequeue();
    q.enqueue(2);
    q.enqueue(4);
    q.enqueue(1);
    q.dequeue();
    q.enqueue(6);
    q.dequeue();
    q.enqueue(5);

    cout << "============================================== \n";
    q.display();
    cout << "============================================== \n";
}