#include <iostream>
#include <sstream>
#include <string>
using namespace std;

struct Menus{
  string nama_hidangan;
  int harga;
};

struct Chart{
  string nama_hidangan;
  int qty;
};


int main() {
  Chart pesanan[5];
  Menus menu1[5];
  // char arr_pesanan[100];
  string str_pesanan;

  // deklarasi variable menu1 untuk menampung informasi menu dan harga yang ada
  menu1[0].nama_hidangan = "Soto";
  menu1[0].harga = 15000;
  menu1[1].nama_hidangan = "Rawon";
  menu1[1].harga = 20000;
  menu1[2].nama_hidangan = "Pecel";
  menu1[2].harga = 10000;
  menu1[3].nama_hidangan = "Bakso";
  menu1[3].harga = 12500;
  menu1[4].nama_hidangan = "Siomay";
  menu1[4].harga = 25000;

  // prompt user untuk melakukan input pesanan
  cout << "Isikan Makanan yang dipesan : ";
  getline(cin, str_pesanan);

  // convert string ke array
  stringstream ssin(str_pesanan);
  int i = 0;
  int loop = 0;
  while (ssin.good() && i < 5){
    ssin >> pesanan[i].nama_hidangan;
    ssin >> pesanan[i].qty;
    i++;
    loop++;
  }
  // debug untuk melihat isi variable pesanan
  // for(i = 0; i < loop; i++){
  //   cout << pesanan[i].nama_hidangan << endl;
  //   cout << pesanan[i].qty << endl;
  // }

  // set output untuk ditampilkan di layar
  cout << "Total Harga : \n";
  int harga_total_per_hidangan, total;
  for(int i = 0; i < loop; i++){
    for(int j = 0; j < 5; j++){
      if(pesanan[i].nama_hidangan == menu1[j].nama_hidangan){
        harga_total_per_hidangan = menu1[j].harga * pesanan[i].qty;
        cout << " - " << pesanan[i].nama_hidangan << " @" << menu1[j].harga << " * " << pesanan[i].qty << " = " << harga_total_per_hidangan << endl;
        total = total + harga_total_per_hidangan;
      }
    }
  }
  cout << "Total = " << total << endl;
}