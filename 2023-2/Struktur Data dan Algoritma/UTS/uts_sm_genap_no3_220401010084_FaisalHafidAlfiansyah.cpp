#include <iostream>
using namespace std;

#define MAX 5

class Stack{
  private:
    int stack[MAX];
    int top;

  public:
    Stack(){
      top = -1;
    };

    void push(int elemen){
      if(top >= MAX - 1){
        cout << "Stack overflow.\n"; 
      }else{
        top++;
        stack[top] = elemen;
      }
    }

    int pop(){
      if(top < 0){
        cout << "Stack underflow.\n";
        return 0;
      }else{
        int item = stack[top];
        top--;
        return item;
      }
    }

    int peek(){
      if(top < 0){
        cout << "Stack Kosong";
        return 0;
      }else{
        int item = stack[top];
        return item;
      }
    }

    bool isEmpty(){
      return (top < 0);
    }

    void display(){
      if(isEmpty()){
        cout << "Stack Kosong.\n";
      }else{
        int i;
        for(i = 0; i < top; i++){
          cout << stack[i] << endl;
        }
        cout << stack[i] << endl;
      }
    }
};

int main() {
  Stack stack1;
  Stack stack2;

  // push 5 angka ke stack pertama 
  stack1.push(1);
  stack1.push(2);
  stack1.push(3);
  stack1.push(4);
  stack1.push(5);
  
  // push 5 angka ke stack kedua
  stack2.push(1);
  stack2.push(2);
  stack2.push(3);
  stack2.push(9);
  stack2.push(10);

  // pengecekan isi stack sama atau tidak
  for(int i = 0; i < MAX; i++){
    if(stack1.pop() != stack2.pop()){
      cout << "Dua stack tersebut memiliki nilai yang berbeda.\n";
      break;
    } else {
      if(i == MAX - 1){
        cout << "Dua stack tersebut memiliki nilai yang sama.\n";
      }
    }
  }
}